package gotest

import (
	"reflect"
	"testing"
)

func assertEqual(t *testing.T, lhs interface{}, rhs interface{}) {
	t.Helper()
	if !reflect.DeepEqual(lhs, rhs) {
		t.Errorf("values are not equal: %v != %v", lhs, rhs)
	}
}

func TestAssert(t *testing.T) {

	t.Run("equality check of equal values", func(t *testing.T) {
		tm := createTestMock(t)
		tc := GoTest(tm)
		assert := makeAssert(tc)
		assert.Equal(123, 123)
		tm.assertCalledTimes(1, "Helper")
		assertEqual(t, assert.failCount, 0)
	})

	t.Run("equality check of non-equal values", func(t *testing.T) {
		tm := createTestMock(t)
		tc := GoTest(tm)
		assert := makeAssert(tc)
		assert.Equal("foo", 123)
		tm.assertCalledTimes(1, "Helper")
		tm.assertCalledTimes(1, "Log", "values are not equal: foo != 123")
		assertEqual(t, assert.failCount, 1)
	})

	t.Run("inequality check of inequal values", func(t *testing.T) {
		tm := createTestMock(t)
		tc := GoTest(tm)
		assert := makeAssert(tc)
		assert.NotEqual("foo", 123)
		tm.assertCalledTimes(1, "Helper")
		assertEqual(t, assert.failCount, 0)
	})

	t.Run("inequality check of equal values", func(t *testing.T) {
		tm := createTestMock(t)
		tc := GoTest(tm)
		assert := makeAssert(tc)
		assert.NotEqual(123, 123)
		tm.assertCalledTimes(1, "Helper")
		tm.assertCalledTimes(1, "Log", "values are equal: 123 == 123")
		assertEqual(t, assert.failCount, 1)
	})

	t.Run("trueness test of value being true", func(t *testing.T) {
		tm := createTestMock(t)
		tc := GoTest(tm)
		assert := makeAssert(tc)
		assert.True(true)
		tm.assertCalledTimes(1, "Helper")
		assertEqual(t, assert.failCount, 0)
	})

	t.Run("trueness test of value being false", func(t *testing.T) {
		tm := createTestMock(t)
		tc := GoTest(tm)
		assert := makeAssert(tc)
		assert.True(false)
		tm.assertCalledTimes(1, "Helper")
		tm.assertCalledTimes(1, "Log", "value is not true")
		assertEqual(t, assert.failCount, 1)
	})

	t.Run("falseness test of value being false", func(t *testing.T) {
		tm := createTestMock(t)
		tc := GoTest(tm)
		assert := makeAssert(tc)
		assert.False(false)
		tm.assertCalledTimes(1, "Helper")
		assertEqual(t, assert.failCount, 0)
	})

	t.Run("falseness test of value being true", func(t *testing.T) {
		tm := createTestMock(t)
		tc := GoTest(tm)
		assert := makeAssert(tc)
		assert.False(true)
		tm.assertCalledTimes(1, "Helper")
		tm.assertCalledTimes(1, "Log", "value is not false")
		assertEqual(t, assert.failCount, 1)
	})
}
