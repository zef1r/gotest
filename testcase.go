package gotest

import (
	"fmt"
	"reflect"
	"testing"
)

// TestFunc Type of test function
type TestFunc func(assert *Assert)

// TestCase A structure that keeps all recorded tests
type TestCase struct {
	t     ITest
	tests map[string]TestFunc
}

// Test Record test
func (tc *TestCase) Test(name string, testFunc TestFunc) {
	if _, found := tc.tests[name]; found {
		tc.t.Fatal(fmt.Sprintf("test case already exists: %v", name))
	} else {
		tc.tests[name] = testFunc
	}
}

// Run Run all recorded tests
func (tc *TestCase) Run() {
	for name, testFunc := range tc.tests {
		tc.t.Run(name, func(t *testing.T) {
			assert := makeAssert(tc)
			testFunc(assert)
			if assert.failCount > 0 {
				tc.t.Fail()
			}
		})
	}
}

type testMock struct {
	t         *testing.T
	funcCalls map[string][][]interface{}
}

func createTestMock(t *testing.T) *testMock {
	return &testMock{
		t:         t,
		funcCalls: make(map[string][][]interface{}),
	}
}

func (tm *testMock) Log(args ...interface{}) {
	var newArgs []interface{}
	newArgs = append(newArgs, args...)
	tm.funcCalls["Log"] = append(tm.funcCalls["Log"], newArgs)
}

func (tm *testMock) Run(name string, testFunc func(t *testing.T)) bool {
	var args []interface{}
	args = append(args, name)
	tm.funcCalls["Run"] = append(tm.funcCalls["Run"], args)
	testFunc(tm.t)
	return true
}

func (tm *testMock) Helper() {
	var args []interface{}
	tm.funcCalls["Helper"] = append(tm.funcCalls["Helper"], args)
}

func (tm *testMock) Fail() {
	var args []interface{}
	tm.funcCalls["Fail"] = append(tm.funcCalls["Fail"], args)
}

func (tm *testMock) Fatal(args ...interface{}) {
	var newArgs []interface{}
	newArgs = append(newArgs, args...)
	tm.funcCalls["Fatal"] = append(tm.funcCalls["Fatal"], newArgs)
}

func (tm *testMock) assertCalledTimes(expectedCount int, funcName string, args ...interface{}) {
	tm.t.Helper()
	actualCount := 0
	var expectedArgs []interface{}
	expectedArgs = append(expectedArgs, args...)
	argsList, found := tm.funcCalls[funcName]
	if found {
		for _, actualArgs := range argsList {
			if reflect.DeepEqual(actualArgs, expectedArgs) {
				actualCount++
			}
		}
	}
	if actualCount != expectedCount {
		signature := fmt.Sprintf("%v(%v)", funcName, expectedArgs) // TODO: format function call
		tm.t.Errorf(
			"%v: expected to be called %v times, but was called %v times",
			signature, expectedCount, actualCount)
	}
}
