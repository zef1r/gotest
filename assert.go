package gotest

import (
	"fmt"
	"reflect"
)

// Assert Structure that keeps track of checked assertions
type Assert struct {
	tc        *TestCase
	failCount int
}

func makeAssert(tc *TestCase) *Assert {
	return &Assert{
		tc:        tc,
		failCount: 0,
	}
}

// Equal Check if two values are equal
func (a *Assert) Equal(lhs interface{}, rhs interface{}) {
	a.tc.t.Helper()
	if !reflect.DeepEqual(lhs, rhs) {
		message := fmt.Sprintf("values are not equal: %v != %v", lhs, rhs)
		a.tc.t.Log(message)
		a.failCount++
	}
}

// NotEqual Check if two values are not equal
func (a *Assert) NotEqual(lhs interface{}, rhs interface{}) {
	a.tc.t.Helper()
	if reflect.DeepEqual(lhs, rhs) {
		message := fmt.Sprintf("values are equal: %v == %v", lhs, rhs)
		a.tc.t.Log(message)
		a.failCount++
	}
}

// True checks if given boolean value is true
func (a *Assert) True(value bool) {
	a.tc.t.Helper()
	if !value {
		a.tc.t.Log("value is not true")
		a.failCount++
	}
}

// False checks if given boolean value is false
func (a *Assert) False(value bool) {
	a.tc.t.Helper()
	if value {
		a.tc.t.Log("value is not false")
		a.failCount++
	}
}
