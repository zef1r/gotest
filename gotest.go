package gotest

import "testing"

// ITest A base interface test case depends on.
type ITest interface {
	Log(args ...interface{})
	Run(name string, testFunc func(t *testing.T)) bool
	Helper()
	Fail()
	Fatal(args ...interface{})
}

// GoTest Library entry point.
// It creates TestCase objects that can later be used to define tests
func GoTest(t ITest) *TestCase {
	return &TestCase{
		t:     t,
		tests: make(map[string]TestFunc),
	}
}
