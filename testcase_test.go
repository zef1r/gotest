package gotest

import (
	"testing"
)

func TestTestCase(t *testing.T) {

	t.Run("run one empty test", func(t *testing.T) {
		tm := createTestMock(t)
		tc := GoTest(tm)
		tc.Test("first", func(assert *Assert) {})
		tc.Run()
		tm.assertCalledTimes(1, "Run", "first")
	})

	t.Run("run two empty tests", func(t *testing.T) {
		tm := createTestMock(t)
		tc := GoTest(tm)
		tc.Test("first", func(assert *Assert) {})
		tc.Test("second", func(assert *Assert) {})
		tc.Run()
		tm.assertCalledTimes(1, "Run", "first")
		tm.assertCalledTimes(1, "Run", "second")
	})

	t.Run("recording two tests with same name makes test fail with fatal", func(t *testing.T) {
		tm := createTestMock(t)
		tc := GoTest(tm)
		tc.Test("first", func(assert *Assert) {})
		tc.Test("first", func(assert *Assert) {})
		tm.assertCalledTimes(1, "Fatal", "test case already exists: first")
	})

	t.Run("when test fails then parent test fails", func(t *testing.T) {
		tm := createTestMock(t)
		tc := GoTest(tm)
		tc.Test("first", func(assert *Assert) {
			assert.Equal(1, 2)
		})
		tc.Run()
		tm.assertCalledTimes(1, "Run", "first")
		tm.assertCalledTimes(1, "Fail")
		tm.assertCalledTimes(1, "Helper")
		tm.assertCalledTimes(1, "Log", "values are not equal: 1 != 2")
	})
}
