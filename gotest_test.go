package gotest

import "testing"

func TestRunSinglePassingTest(t *testing.T) {
	gt := GoTest(t)

	gt.Test("first", func(assert *Assert) {
		assert.Equal(123, 123)
	})

	gt.Run()
}

func TestRunTwoPassingTests(t *testing.T) {
	gt := GoTest(t)

	gt.Test("first", func(assert *Assert) {
		assert.Equal(123, 123)
	})

	gt.Test("second", func(assert *Assert) {
		assert.Equal(123, 123)
	})

	gt.Run()
}
